$(document).ready(function() {
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange

    if (!window.indexedDB) {
        window.alert("your browser doesn't support a stable version of IndexedDB.")
    }
    var db
    var request = window.indexedDB.open("dabajabaza.github.io", 1)
    request.onerror = function(event) {
        console.log("error: ")
    }
    var pageX, pageY
    var filler = "…"

    $(window).on("mousemove", function(e){
        pageX = e.pageX
        pageY = e.pageY
    })
    
    function addRecord(editCell) {
      cells = editCell.closest("tr").find("td")
      var newRecord = {}
      var flag = true
      cells.each(function() {
        var value = $.trim($(this).text())
        var key = $(this).attr("class").split("__")[1]
        if (value != filler) {
            newRecord[key] = value
        } else {
            flag = false
        }
      })
      if (flag) {
        delete newRecord.manipulate_row
        addData(newRecord)
      } else {
          console.log("all fields are required!")
      }
    }
    
    $("table").on("click, focus", ".table_product__category", function() {
        var floatList = $(".float_list")
        cell = $("div", this)
        var originVal = cell.text()
        floatList.show().css({
            top: pageY,
            left: pageX
        })
        $("li").on("click", function() {
            cell.text($(this).text())
            floatList.hide()
            if (!$(this).text()) editCell.text(filler)
            if ($(this).text() == originVal) return false
            addRecord(cell.parent())
        })
    })

    //Edit cells and save result in db
    $("table").on("click, focus", "td:not('.table_product__category')", function() {
        var tr, td
            //for debug purpose//
        tr = $(this).parent().index()
        td = $(this).index()
        console.log("row# ", tr, "cell# ", td)
            ////////////////////
        var editCell = $(".table_product__editable_box", this)
        var originVal = editCell.text()
        if (editCell.text() == filler) editCell.text("")
        editCell.on("keyup", function(e) {
            var val = editCell.text()
        })
        editCell.on("focusout", function() {
            if (!editCell.text()) editCell.text(filler)
            if (editCell.text() == originVal) return false
            addRecord(editCell)
        })
    })


    //Show/hide removal button
    $("table").on("mouseenter", "tr", function() {
        var removeRow = $(this).children().last().children("div")
        removeRow.show()
        $(this).on("mouseleave", function() {
            removeRow.hide()
        })
    })

    //Adding new row and get new id
    $("table").on("click", ".table_product__manipulate_row__add", function() {
        var lastTr = $("tr").last()
        lastTr
            .clone()
            .appendTo("table")
            .find(".table_product__editable_box")
            .html(filler)
        var idCell = $("td", lastTr.next()).first()
        var newId = (idCell.text() ^ 0) + 1
        idCell.text(newId)
    })

    //Remove selected row from db and DOM
    $("table").on("click", ".table_product__manipulate_row__remove", function() {
        var markedForRemoval = $(this).parent()
        var id = markedForRemoval.siblings(":first").text()
        removeData(id)
        markedForRemoval.parent().remove()
    })


    ///////////  DB MANIPULATION ///////////

    //Read all data from db and fill a table
    request.onsuccess = function(event) {
        db = request.result
        console.log("success: " + db)
        readAllData(function(items) {
            var numberOfRecords = items.length
            if (numberOfRecords == 0) items = initialRecords
            var row = $("tr").last()
            for (i in items) {
                var cl = row.last().clone()
                cl.each(function() {
                    $("td", this).first().text(items[i].id)
                        .next().find("div").text(items[i].title)
                        .parent().next().find("div").text(items[i].category)
                        .parent().next().find("div").text(items[i].price)
                })
                cl.appendTo("table")
                addData(items[i])
            }
        })
    }

    //
    request.onupgradeneeded = function(event) {
        var db = event.target.result
        var objectStore = db.createObjectStore("products", {
            keyPath: "id"
        })
        
    }

    //Adding or update record
    function addData(json) {
        var request = db.transaction(["products"], "readwrite")
            .objectStore("products")
            .put(json)
            //.add(json)
        request.onsuccess = function(event) {
            console.log("the record has been added or updated")
        }
    }

    //Remove selected record
    function removeData(id) {
        var request = db.transaction(["products"], "readwrite")
            .objectStore("products")
            .delete(id)
        request.onsuccess = function(event) {
            console.log("Record has been deleted")
        }
    }

    //Read all records from DB
    function readAllData(callback) {
        var trans = db.transaction("products", IDBTransaction.READ_ONLY)
        var store = trans.objectStore("products")
        var items = []
        trans.oncomplete = function(evt) {
            callback(items)
        }
        var cursorRequest = store.openCursor()
        cursorRequest.onerror = function(error) {
            console.log(error)
        }
        cursorRequest.onsuccess = function(evt) {
            var cursor = evt.target.result
            if (cursor) {
                items.push(cursor.value)
                cursor.continue()
            }
        }
        return items
    }
    
    //Test data
    const initialRecords = [
      { id: "1", title: "Фуриазол", category: "Стимуляторы", price: "20 сепулек" },
      { id: "2", title: "Двуодурь благотворина", category: "Седативные", price: "15 мягких пчм" },
      { id: "3", title: "Альтруизин", category: "Эйфорики", price: "10 муркви" },
      { id: "4", title: "Сепулька", category: "Сепуляторы", price: "2 сепульки" },
    ]
})